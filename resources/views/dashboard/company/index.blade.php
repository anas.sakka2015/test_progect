@extends('layouts.app')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">{{ __('cms.tables') }} /</span>
            {{ __('cms.company') }}</h4>

        <!-- Basic Bootstrap Table -->

        <div class="card h-100">
            <div class="row">
                <div class="col-lg-10 col-12">
                    <h5 class="card-header">{{ __('cms.company') }} {{ __('cms.table') }} </h5>
                </div>
                <div class="col-lg-2 col-12 p-3">
                    <a href="{{ route('admin.companies.create') }}" class="btn btn-primary btn-block w-100 ">
                        Add company
                    </a>
                </div>
            </div>
            <div class="table-responsive text-nowrap h-100">

                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Logo</th>
                            <th>Website</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-1 ">
                        @include('dashboard.company.table')
                    </tbody>
                </table>
                <div class="row align-items-center text-center mt-5">
                    <div class="col-12 col-lg-4 "></div>
                    <div class="col-12 col-lg-4 text-center ">
                        <div class="pageination text-center ">
                            {!! $companies->links() !!}
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 "></div>
                </div>
            </div>
        </div>
        <hr class="my-5" />
    </div>
@endsection
