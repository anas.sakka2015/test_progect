@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <form action="{{ route('admin.companies.store') }}" method="post" class="p-4" enctype="multipart/form-data">
                @csrf
                @method('post')
                <h5 class="card-header">Add New Company </h5>
                <div class="card-body demo-vertical-spacing demo-only-element">
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAP1JREFUSEvtltENwjAMRF8ngA1gBSaATWATYANGYBNgA0aASUCHMLKqRCLFNCA1Un+syC++nN00VFpNJS4DuDfl21KvgDUwDT7BBdgCe8vrwQvgEAxsp5sBZwU9ePOsVnGdLnJJRcsrThYc7fbbX4FThjsB8oStHbAExi6m63rIChRXnDOcB3tveF98BM4ZTu1hrXEE5sDVtwuguL5OFXtwznAGbsvvqy+WegCnhsrXpbbWENwbycDecNqjO+9srmrtpJNrgKjaidM5NUC0bxQ1QKJ+FMXt1DvYGykCbvleY/QnHgI5I0VUrPZT1cmnTwTg7RzRL40BnFWgmtR3GzFkH/kaNR0AAAAASUVORK5CYII=" />
                        <input type="text" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                            placeholder="name" aria-label="name" value="{{ old('name') }}" name='name' />
                        @if ($errors->has('name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    </div>

                    <div class="input-group">

                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAUNJREFUSEvN1+FtwjAQhuGXDToCTNBuUDoBYgQmaDdomQBGKBswAiPABHSDlg2qTyLIoPh856Sk/oMQdh5/F8cxIwZqo4FcbuExMAMeHBP6ATaAPsMthYUeg1f4AubAPjjuKvEbsIpe4Jz4JYqniT+A9wpYQ1TuEG7BJ2AN7M73XBN7NCYWwi14CQhrmhbcd6EibtyCVTqlTZu+P/eBW7BW6zaYuOleTG7BekSUWhdRmV9vSl9ahybuWdUq77SkZH7P4h640rwMa8XvAWsG2uEmaYIUrt25vBVZAJ9N5xTWAtL9tDYJL9LWLwur81/hB+ApV+p0h+ozuVA9FVevz9xBoK/kragSWieQrngWLcFd7rmJeuAavIh64QjuQiOwB3ejUdjCQ2gN3IaH0Vq4wbW3q+lcFj5b/5t/El1eAqGxgyX+BeC9Ux+v9IFjAAAAAElFTkSuQmCC" />
                        <input type="email" required class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                            placeholder="email" aria-label="email" name="email" value="{{ old('email') }}" />
                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input-group">

                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAUNJREFUSEvN1+FtwjAQhuGXDToCTNBuUDoBYgQmaDdomQBGKBswAiPABHSDlg2qTyLIoPh856Sk/oMQdh5/F8cxIwZqo4FcbuExMAMeHBP6ATaAPsMthYUeg1f4AubAPjjuKvEbsIpe4Jz4JYqniT+A9wpYQ1TuEG7BJ2AN7M73XBN7NCYWwi14CQhrmhbcd6EibtyCVTqlTZu+P/eBW7BW6zaYuOleTG7BekSUWhdRmV9vSl9ahybuWdUq77SkZH7P4h640rwMa8XvAWsG2uEmaYIUrt25vBVZAJ9N5xTWAtL9tDYJL9LWLwur81/hB+ApV+p0h+ozuVA9FVevz9xBoK/kragSWieQrngWLcFd7rmJeuAavIh64QjuQiOwB3ejUdjCQ2gN3IaH0Vq4wbW3q+lcFj5b/5t/El1eAqGxgyX+BeC9Ux+v9IFjAAAAAElFTkSuQmCC" />
                        <input type="text" required
                            class="form-control {{ $errors->has('website') ? ' is-invalid' : '' }}" placeholder="website"
                            aria-label="website" name="website" value="{{ old('website') }}" />
                        @if ($errors->has('website'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('website') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input-group">
                        <img src="{{ asset('upload.png') }}" id="output" class="rounded my-2" alt=""
                            width="150px" height="150px">
                        <label class="form-label w-100 " for="basic-default-password12">logo</label><br>
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAPVJREFUSEvtl40RwUAQhb90oAM6QAXogA6UoAQ6oQI6QAdKoAJKMC9zMXETdzkuMsztTCaTZPPe7mb/ktGSZC3x4iKeAv0Iht2ADaDzQ6qIB8AW6EUgLSBOwNBHLKUYntp2i1jYudged4Drm54egbH1rq735t4EOLwiLiuG8idiRSyF+j+TS41nZLrVV76xmoOOuanDM7ADFk3Xsa/uG2sgidiOQAr1U0Q+mU6/n1wXYO1zI+C5sNRUKjeQcqhnpvMEYNdXda0+S2BVHypMs2rLbGrZc9axzNZ00dzshvng1fYSFwha6LVjxxJncsUi8eK09u90Bw1yTB+z/MtBAAAAAElFTkSuQmCC" />
                        <input type="file"
                            onchange="document.getElementById('output').src = window.URL.createObjectURL(this.files[0])"
                            class="form-control" placeholder="logo" aria-label="logo" name="logo" value="" />
                    </div>
                    <button class="btn btn-primary btn-style " type="submit">Add</button>
                </div>
            </form>
        </div>
    </div>
@endsection
