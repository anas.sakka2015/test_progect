@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <form action="{{ route('admin.employees.store') }}" method="post" class="p-4" enctype="multipart/form-data">
                @csrf
                @method('post')
                <h5 class="card-header">Add New Employee </h5>
                <div class="card-body demo-vertical-spacing demo-only-element">
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAP1JREFUSEvtltENwjAMRF8ngA1gBSaATWATYANGYBNgA0aASUCHMLKqRCLFNCA1Un+syC++nN00VFpNJS4DuDfl21KvgDUwDT7BBdgCe8vrwQvgEAxsp5sBZwU9ePOsVnGdLnJJRcsrThYc7fbbX4FThjsB8oStHbAExi6m63rIChRXnDOcB3tveF98BM4ZTu1hrXEE5sDVtwuguL5OFXtwznAGbsvvqy+WegCnhsrXpbbWENwbycDecNqjO+9srmrtpJNrgKjaidM5NUC0bxQ1QKJ+FMXt1DvYGykCbvleY/QnHgI5I0VUrPZT1cmnTwTg7RzRL40BnFWgmtR3GzFkH/kaNR0AAAAASUVORK5CYII=" />
                        <input type="text" required
                            class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                            placeholder="first_name" aria-label="first_name" value="{{ old('first_name') }}"
                            name='first_name' />
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif

                    </div>
                    <div class="input-group">
                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAP1JREFUSEvtltENwjAMRF8ngA1gBSaATWATYANGYBNgA0aASUCHMLKqRCLFNCA1Un+syC++nN00VFpNJS4DuDfl21KvgDUwDT7BBdgCe8vrwQvgEAxsp5sBZwU9ePOsVnGdLnJJRcsrThYc7fbbX4FThjsB8oStHbAExi6m63rIChRXnDOcB3tveF98BM4ZTu1hrXEE5sDVtwuguL5OFXtwznAGbsvvqy+WegCnhsrXpbbWENwbycDecNqjO+9srmrtpJNrgKjaidM5NUC0bxQ1QKJ+FMXt1DvYGykCbvleY/QnHgI5I0VUrPZT1cmnTwTg7RzRL40BnFWgmtR3GzFkH/kaNR0AAAAASUVORK5CYII=" />
                        <input type="text" required
                            class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                            placeholder="last_name" aria-label="last_name" value="{{ old('last_name') }}"
                            name='last_name' />
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif

                    </div>

                    <div class="input-group">

                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAUNJREFUSEvN1+FtwjAQhuGXDToCTNBuUDoBYgQmaDdomQBGKBswAiPABHSDlg2qTyLIoPh856Sk/oMQdh5/F8cxIwZqo4FcbuExMAMeHBP6ATaAPsMthYUeg1f4AubAPjjuKvEbsIpe4Jz4JYqniT+A9wpYQ1TuEG7BJ2AN7M73XBN7NCYWwi14CQhrmhbcd6EibtyCVTqlTZu+P/eBW7BW6zaYuOleTG7BekSUWhdRmV9vSl9ahybuWdUq77SkZH7P4h640rwMa8XvAWsG2uEmaYIUrt25vBVZAJ9N5xTWAtL9tDYJL9LWLwur81/hB+ApV+p0h+ozuVA9FVevz9xBoK/kragSWieQrngWLcFd7rmJeuAavIh64QjuQiOwB3ejUdjCQ2gN3IaH0Vq4wbW3q+lcFj5b/5t/El1eAqGxgyX+BeC9Ux+v9IFjAAAAAElFTkSuQmCC" />
                        <input type="email" required class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                            placeholder="email" aria-label="email" name="email" value="{{ old('email') }}" />
                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input-group">

                        <img class="input-group-text" width="55px"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAUNJREFUSEvN1+FtwjAQhuGXDToCTNBuUDoBYgQmaDdomQBGKBswAiPABHSDlg2qTyLIoPh856Sk/oMQdh5/F8cxIwZqo4FcbuExMAMeHBP6ATaAPsMthYUeg1f4AubAPjjuKvEbsIpe4Jz4JYqniT+A9wpYQ1TuEG7BJ2AN7M73XBN7NCYWwi14CQhrmhbcd6EibtyCVTqlTZu+P/eBW7BW6zaYuOleTG7BekSUWhdRmV9vSl9ahybuWdUq77SkZH7P4h640rwMa8XvAWsG2uEmaYIUrt25vBVZAJ9N5xTWAtL9tDYJL9LWLwur81/hB+ApV+p0h+ozuVA9FVevz9xBoK/kragSWieQrngWLcFd7rmJeuAavIh64QjuQiOwB3ejUdjCQ2gN3IaH0Vq4wbW3q+lcFj5b/5t/El1eAqGxgyX+BeC9Ux+v9IFjAAAAAElFTkSuQmCC" />
                        <input type="number" required
                            class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="phone"
                            aria-label="phone" name="phone" value="{{ old('phone') }}" />
                        @if ($errors->has('phone'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group">

                        <select name="company_id"
                            class="form-select form-select-lg {{ $errors->has('company_id') ? ' is-invalid' : '' }}">
                            <option value="">
                                select company
                            </option>
                            @foreach ($companies as $company)
                                <option value="{{ $company->id }}" {{ old('company_id') ? 'selected' : '' }}>
                                    {{ $company->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('company_id'))
                            <span class="invalid-feedback text-right" style="display: block;" role="alert">
                                <strong>{{ $errors->first('company_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button class="btn btn-primary btn-style " type="submit">Add</button>
                </div>
            </form>
        </div>
    </div>
@endsection
