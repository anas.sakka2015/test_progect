<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => 'nullable|email|unique:companies,email,'.$this->employee->id,
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'company_id' => 'required|exists:companies,id',
            'phone' => 'nullable|numeric|min:10|unique:companies,email,'.$this->employee->id,
        ];
    }
}
