<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;
use Illuminate\Support\Facades\App;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        $data = $request->validated();
        if (!Auth::attempt($data)) {
            return redirect()->back()->withInput($request->validated())->with('error', 'Your password is incorrect');
        }
        return redirect()->route('admin.companies.index')->with('success', 'Login successfully');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('success', 'Logout successfully');
    }
    public function home()
    {
        return redirect()->route('login.index');
    }
    public function dashboard()
    {
        return redirect()->route('admin.companies.index');
    }
}
