<?php

namespace App\Repositories;

use App\Models\Company;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Storage;
use App\Repositories\Repository;
//use Your Model

/**
 * Class CompanyRepository.
 */
class CompanyRepository extends Repository
{
    protected $model;
    public function __construct(Company $model)
    {
        $this->model = $model;
    }
    public function create(array $attributes)
    {
        if (array_key_exists('logo', $attributes)) {
            $logo = $attributes['logo']->storePublicly('company', 'public');
            $attributes['logo'] = $logo;
        }
        $company = parent::create(array_merge($attributes));
        return $company;
    }
    public function update($id, array $attributes)
    {
        $company =  $this->model->find($id);
        if (array_key_exists('logo', $attributes)) {
            if ($company->logo && Storage::exists('public/' . $company->logo)) {
                Storage::delete('public/' . $company->logo);
            }
            $logo = $attributes['logo']->storePublicly('company', 'public');
            $attributes['logo'] = $logo;
        }
        $company = $company->update(array_merge($attributes));
        return $company;
    }
    public function delete($id)
    {
        $company =  $this->model->find($id);
        if ($company->logo && Storage::exists('public/' . $company->logo)) {
            Storage::delete('public/' . $company->logo);
        }
        $company = $company->delete();
        return true;
    }
}
